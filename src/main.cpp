#include <SD.h>
#include <SPI.h>
#include <DS3231.h>
#include <Wire.h>
#include <Adafruit_ADS1015.h>

Adafruit_ADS1015 ads; // Analog to Digital Converter Declaration 
DS3231  rtc(2, 3);    // Real time Clock Declaration
File myFile;          // Micro SD card Declaration
int SDCSpin = 4;      // Micro SD chip select pin
int ledPin = 17;      // Promicro led pin


float multiplier;     // Analog to Digital converter multipying factor
int loggingInterval = 3000;  // Logging interval in milisecond

void setup() {
    
  Serial.begin(9600); // Initialize Serial for debuging purpose
  while(!Serial);     // Wait Serial ready

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  
  rtc.begin();        // Real time clock Initialization
  Serial.println("Real Time Clock Ready");

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //
  ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  multiplier = 3.0F; // according to gain selection
  ads.begin();       // Initialization of Analog to Digital Converter
  Serial.println("Analog to Digital Converter Ready");

  // SD Card Initialization and check
  if (SD.begin(SDCSpin))
  {
    Serial.println("SD card is ready to use.");
  } else
  {
    Serial.println("SD card initialization failed");
    return;
  }
  digitalWrite(ledPin, LOW);
}
void loop() {
  // Data logging starts here
  myFile = SD.open("LogFile.txt", FILE_WRITE); // Open Logging file 
  if (myFile) {
    digitalWrite(ledPin, LOW);
    String time = rtc.getTimeStr(); // Get time from Real Time Clock
    String date = rtc.getDateStr(); // Get date from Real Time Clock
    float voltage1 = ads.readADC_Differential_0_1() * multiplier; // Get voltage input from Analog to Digital Converter
    float voltage2 = ads.readADC_Differential_2_3() * multiplier; // Get voltage input from Analog to Digital Converter
    float temp = rtc.getTemp(); // Get temperature reading from RTC

    // Store time and voltage Data
    myFile.print(date);      // Store time reading
    myFile.print(" ");      // Add delimiter
    myFile.print(time);      // Store date reading
    myFile.print(", ");      // Add delimiter
    myFile.print(voltage1); // Store voltage reading from channel 1
    myFile.print(", ");      // Add delimiter
    myFile.print(voltage2); // Store voltage reading from channel 2
    myFile.print(", ");      // Add delimiter
    myFile.println(temp);
    myFile.close(); // close the file

    Serial.print(date); Serial.print(" ");
    Serial.print(time); Serial.print(", ");
    Serial.print(voltage1); Serial.print(", ");
    Serial.print(voltage2); Serial.print(", ");
    Serial.println(temp);
  }

  // if the file didn't open, print an error:
  else {
    Serial.println("error opening test.txt");
    digitalWrite(ledPin, HIGH);
  }

  digitalWrite(ledPin, HIGH);
  delay(loggingInterval);
}